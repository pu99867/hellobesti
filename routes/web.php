<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('back/dashboard/index');
// });
Route::get('/login',[LoginController::class,'index'])->name('login');
Route::post('/login-proses',[LoginController::class,'login_proses'])->name('login-proses');
Route::post('/logout',[LoginController::class,'logout'])->name('logout');


Route::group(['prepix' => 'admin','middleware' => ['auth'],'as'=>'admin.'], function(){
    Route::get('/dashboard',[DashboardController::class,'index'])->name('dashboard');
    Route::resource('/teams', \App\Http\Controllers\TeamController::class);
    Route::resource('/contacts', \App\Http\Controllers\ContactController::class);
    Route::resource('/evolutions', \App\Http\Controllers\EvolutionController::class);
    Route::resource('/company_goals', \App\Http\Controllers\CompanyGoalsController::class);
    Route::resource('/settings', \App\Http\Controllers\SettingController::class);
    Route::resource('/abouts', \App\Http\Controllers\AboutController::class);
    Route::resource('/clients', \App\Http\Controllers\ClientController::class);
    Route::resource('/homes', \App\Http\Controllers\HomeController::class);

});

Route::get('/register', [LoginController::class, 'register'])->name('register');

Route::post('/register', [LoginController::class, 'register_proses']);