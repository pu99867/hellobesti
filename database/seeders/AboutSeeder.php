<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //

        DB::table('abouts')->insert([
            'desc_1' => 'We are here as a digital company that aims to be the most complete and professional digital service provider in Indonesia, we care about our clients.',
            'desc_2' => 'We come as your partner who helps and provides solutions for developing digital marketing platforms, especially social media which is currently developing. And we are the only company that provides #Trending Topic Twitter services since 2011 with guarantee.',
            'image' => '',
            'vision' => 'To be The Leading Provider of Top-Quality and Professional Digital Company & Promotion Service in The Country ',
            'misi' => 'We do business with integrity, honesty, and professionalism.

We care about our clients and aim to give them the best service possible.

We are committed to supporting your growing business.',
            'count_target' => "500++", // Change this to the desired value
            'count_Digital_Campaign' => "Digital Campaign & Production", // Change this to the desired value
            'count_target_income' => "10M", // Change this to the desired value
            'count_income' => "10 Million annual revenue / One years", // Change this to the desired value
            'flag' => 1
        ]);
    }
}
