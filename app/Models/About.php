<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'desc',
        'desc_about',
        'count_bussines',
        'count_member',
        'count_client',
        'count_project',
        'vision',
        'misi',
        'flag',
    ];
}
