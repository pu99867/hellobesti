<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class home extends Model
{
    use HasFactory;

    protected $fillable = 
    [
        'id',
        'desc_section_1',
        'title_section_2',
        'desc_section_2',
        'image_section_2',
        'icon_image_section_2',
        'flag'
    ];
}
