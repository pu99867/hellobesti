<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\home;
use Illuminate\View\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $homes = home::where('flag', 1)
        ->latest()
        ->paginate(5);

        return view('back/home.index', compact('homes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('back/home.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title_section_1' => 'required',
            'desc_section_1' => 'required',
            'title_section_2' => 'required',
            'sub_title_section_2' => 'required',
            'desc_section_2' => 'required',
            'image_section_2' => 'required|image|mimes:jpeg,jpg,png|max:2048',
            'image_section_1' => 'required|image|mimes:jpeg,jpg,png|max:2048',
        ]);

        $imageSection2 = $request->file('image_section_2');
        $imageSection1 = $request->file('image_section_1');

        // Simpan file gambar img_section_2 ke direktori penyimpanan
        $imagePathSection2 = $imageSection2->storeAs('public/posts', $imageSection2->hashName());

        // Simpan file gambar img_section_1 ke direktori penyimpanan
        $imagePathSection1 = $imageSection1->storeAs('public/posts', $imageSection1->hashName());

        Home::create([
            'title_section_1' => $request->title_section_1,
            'desc_section_1' => $request->desc_section_1,
            'title_section_2' => $request->title_section_2,
            'sub_title_section_2' => $request->sub_title_section_2,
            'desc_section_2' => $request->desc_section_2,
            'image_section_2' => $imagePathSection2,
            'image_section_1' => $imagePathSection1,
            'flag' => 1,
        ]);

        return redirect()->route('admin.homes.index')->with(['success' => 'Data Berhasil Disimpan!']);
    }

    

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
