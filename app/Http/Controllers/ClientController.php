<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\View\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $clients = client::all();
        return view('back/client.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('back/client.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'logo' => 'required|image',

        ]);

        
        if ($request->hasFile('logo')) {
            $image = $request->file('logo');
            $image->storeAs('public/posts', $image->hashName());
            // Lanjutkan dengan menyimpan data pengaturan atau pembaruan data.
        }

        client::create([
            'logo' => $image->hashName(),
            'flag' => 1
        ]);

        return redirect()->route('admin.clients.index')->with(['success' => 'Data Berhasil Disimpan!']);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $clients = client::find($id);
        return view('back/client.edit', compact('clients'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
         //validate form
         $this->validate($request, [
            'logo'     => 'required|image|mimes:jpeg,jpg,png|max:2048',
            
        ]);

        //get post by ID
        $clients = client::findOrFail($id);

        //check if image is uploaded
        if ($request->hasFile('image')) {

            //upload new image
            $image = $request->file('image');
            $image->storeAs('public/posts', $image->hashName());

            //delete old image
            Storage::delete('public/posts/'.$clients->image);

            //update post with new image
            $clients->update([
                'logo'     => $image->hashName(),
                
            ]);

        } 
        //redirect to index
        return redirect()->route('admin.clients.index')->with(['success' => 'Data Berhasil Diubah!']);
    

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

}
