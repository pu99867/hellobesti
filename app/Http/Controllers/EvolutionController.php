<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\evolution;
use Illuminate\View\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class EvolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    
    public function index()
{
    $evolutions = evolution::where('flag', 1)
                ->latest()
                ->paginate(5);

    return view('back/evolution.index', compact('evolutions'));
}

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('back/evolution.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'years_record' => '',
            'desc' => ''
        ]);

        evolution::create([
            'years_record' => $request->years_record,
            'desc' => $request->desc,
            'flag' => 1
        ]);

        return redirect()->route('admin.evolutions.index')->with(['success' => 'Data Berhasil Disimpan!']);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $evolutions = evolution::findOrFail($id);

        return view('back/evolution.edit', compact('evolutions'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, [
            'years_record' => 'required',
            'desc' => 'required'
        ]);

        $evolutions = evolution::findOrFail($id);


        $evolutions->update([
            'years_record' => $request->years_record,
            'desc' => $request->desc,
            
        ]);

        return redirect()->route('admin.evolutions.index')->with(['success' => 'Data Berhasil di ubah!']);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $evolutions = evolution::findOrFail($id);
    
        // Mengubah nilai flag menjadi 0
        $evolutions->update(['flag' => 0]);
    
        return redirect()->route('admin.evolutions.index')->with(['success' => 'Data berhasil dihapus!']);
    }
}
