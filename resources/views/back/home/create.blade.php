@extends('back.layouts.app')
@section('content')
@push('styles-plugins')

@endpush

@push('styles')
<style>

</style>
@endpush

    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow-sm rounded">
                    <div class="card-body">
                        <form action="{{ route('admin.homes.store') }}" method="POST" enctype="multipart/form-data">
                        
                            @csrf

                            <div class="form-group">
                                <label class="font-weight-bold">title_section_1</label>
                                <input type="text" class="form-control @error('title_section_1') is-invalid @enderror" name="title_section_1" value="{{ old('title_section_1') }}" placeholder="Masukkan title_section_1">
                            
                                <!-- error message untuk title_section_1 -->
                                @error('title_section_1')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label class="font-weight-bold">desc_section_1</label>
                                <textarea class="form-control @error('desc_section_1') is-invalid @enderror" name="desc_section_1" rows="5" placeholder="Masukkan desc_section_1">{{ old('desc_section_1') }}</textarea>
                            
                                <!-- error message untuk desc_section_1 -->
                                @error('desc_section_1')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label class="font-weight-bold">title_section_2</label>
                                <input type="text" class="form-control @error('title_section_2') is-invalid @enderror" name="title_section_2" value="{{ old('title_section_2') }}" placeholder="Masukkan title_section_2">
                            
                                <!-- error message untuk title_section_2 -->
                                @error('title_section_2')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label class="font-weight-bold">sub_title_section_2</label>
                                <input type="text" class="form-control @error('sub_title_section_2') is-invalid @enderror" name="sub_title_section_2" value="{{ old('sub_title_section_2') }}" placeholder="Masukkan sub_title_section_2">
                            
                                <!-- error message untuk sub_title_section_2 -->
                                @error('sub_title_section_2')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label class="font-weight-bold">desc_section_2</label>
                                <textarea class="form-control @error('desc_section_2') is-invalid @enderror" name="desc_section_2" rows="5" placeholder="Masukkan desc_section_2">{{ old('desc_section_2') }}</textarea>
                            
                                <!-- error message untuk desc_section_2 -->
                                @error('desc_section_2')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label class="font-weight-bold">image_section_2</label>
                                <input type="file" class="form-control @error('image_section_2') is-invalid @enderror" name="image_section_2">
                            
                                <!-- error message untuk img_section_2 -->
                                @error('image_section_2')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label class="font-weight-bold">image_section_1</label>
                                <input type="file" class="form-control @error('image_section_1') is-invalid @enderror" name="image_section_1">
                            
                                <!-- error message untuk img_section_1 -->
                                @error('image_section_1')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                            <button type="reset" class="btn btn-md btn-warning">RESET</button>

                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
</script>
@push('scripts-plugins')

@endpush

@push('scripts')
<script>
    
 
</script>
@endpush
@endsection