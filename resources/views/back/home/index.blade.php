@extends('back.layouts.app')

@section('content')
@push('styles-plugins')

@endpush

@push('styles')
<style>

</style>
@endpush

<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div>
                <h3 class="text-center my-4">DATA HOME</h3>
                <h5 class="text-center"><a href="https://santrikoding.com"></a></h5>
                <hr>
            </div>
            <div class="card border-0 shadow-sm rounded">
                <div class="card-body">
                    <a href="{{ route('admin.homes.create') }}" class="btn btn-md btn-success mb-3">ADD HOME</a>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">title_section_1</th>
                                <th scope="col">desc_section_1</th>
                                <th scope="col">title_section_2</th>
                                <th scope="col">sub_title_section_2</th>
                                <th scope="col">desc_section_2</th>
                                <th scope="col">img_section_2</th>
                                <th scope="col">img_section_1</th>
                                <th scope="col">AKSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($homes as $post)
                            <tr>
                                <td>{{ $post->title_section_1 }}</td>
                                <td>{{ $post->desc_section_1 }}</td>
                                <td>{{ $post->title_section_2 }}</td>
                                <td>{{ $post->sub_title_section_2 }}</td>
                                <td>{{ $post->desc_section_2 }}</td>
                                <td class="text-center">
                                    <img src="{{ asset('/storage/posts/'.$post->image_section_2) }}" class="rounded" style="width: 150px">
                                </td>
                                <td class="text-center">
                                    <img src="{{ asset('/storage/posts/'.$post->image_section_1) }}" class="rounded" style="width: 150px">
                                </td>
                                <td class="text-center">
                                    <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="{{ route('admin.homes.destroy', $post->id) }}" method="POST">
                                        <a href="{{ route('admin.homes.edit', $post->id) }}" class="btn btn-sm btn-primary">EDIT</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger">HAPUS</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="8" class="text-center">
                                    <div class="alert alert-danger">
                                        Data Home belum Tersedia.
                                    </div>
                                </td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $homes->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script>
    //message with toastr
    @if(session()->has('success'))
        toastr.success('{{ session('success') }}', 'BERHASIL!');
    @elseif(session()->has('error'))
        toastr.error('{{ session('error') }}', 'GAGAL!');
    @endif
</script>

@push('scripts-plugins')

@endpush

@push('scripts')
<script>
    
</script>
@endpush
@endsection
