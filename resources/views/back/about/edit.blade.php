@extends('back.layouts.app')
@section('content')
@push('styles-plugins')

@endpush

@push('styles')
<style>

</style>
@endpush

<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-0 shadow-sm rounded">
                <div class="card-body">
                    <form action="{{ route('admin.abouts.update', $about->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <!-- Tambahkan field desc_1 -->
                        <div class="form-group">
                            <label class="font-weight-bold">desc_1</label>
                            <textarea class="form-control @error('desc_1') is-invalid @enderror" name="desc_1" rows="5" placeholder="Masukkan desc_1">{{ old('desc_1', $about->desc_1) }}</textarea>
                        
                            <!-- error message untuk desc_1 -->
                            @error('desc_1')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <!-- Tambahkan field desc_2 -->
                        <div class="form-group">
                            <label class="font-weight-bold">desc_2</label>
                            <textarea class="form-control @error('desc_2') is-invalid @enderror" name="desc_2" rows="5" placeholder="Masukkan desc_2">{{ old('desc_2', $about->desc_2) }}</textarea>
                        
                            <!-- error message untuk desc_2 -->
                            @error('desc_2')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <!-- Tambahkan field image -->
                        <div class="form-group">
                            <label class="font-weight-bold">image</label>
                            <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image', $about->image) }}">
                        
                            <!-- error message untuk image -->
                            @error('image')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <!-- Tambahkan field vision -->
                        <div class="form-group">
                            <label class="font-weight-bold">vision</label>
                            <textarea class="form-control @error('vision') is-invalid @enderror" name="vision" rows="5" placeholder="Masukkan vision">{{ old('vision', $about->vision) }}</textarea>
                        
                            <!-- error message untuk vision -->
                            @error('vision')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <!-- Tambahkan field misi -->
                        <div class="form-group">
                            <label class="font-weight-bold">misi</label>
                            <textarea class="form-control @error('misi') is-invalid @enderror" name="misi" rows="5" placeholder="Masukkan misi">{{ old('misi', $about->misi) }}</textarea>
                        
                            <!-- error message untuk misi -->
                            @error('misi')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <!-- Tambahkan field count_target -->
                        <div class="form-group">
                            <label class="font-weight-bold">count_target</label>
                            <input type="text" class="form-control @error('count_target') is-invalid @enderror" name="count_target" value="{{ old('count_target', $about->count_target) }}" placeholder="Masukkan count_target">
                        
                            <!-- error message untuk count_target -->
                            @error('count_target')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <!-- Tambahkan field count_Digital Campaign -->
                        <div class="form-group">
                            <label class="font-weight-bold">count_Digital_Campaign</label>
                            <input type="text" class="form-control @error('count_Digital Campaign') is-invalid @enderror" name="count_Digital Campaign" value="{{ old('count_Digital Campaign', $about->count_Digital_Campaign) }}" placeholder="Masukkan count_Digital Campaign">
                        
                            <!-- error message untuk count_Digital Campaign -->
                            @error('count_Digital Campaign')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <!-- Tambahkan field count_target_income -->
                        <div class="form-group">
                            <label class="font-weight-bold">count_target_income</label>
                            <input type="text" class="form-control @error('count_target_income') is-invalid @enderror" name="count_target_income" value="{{ old('count_target_income', $about->count_target_income) }}" placeholder="Masukkan count_target_income">
                        
                            <!-- error message untuk count_target_income -->
                            @error('count_target_income')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <!-- Tambahkan field count_income -->
                        <div class="form-group">
                            <label class="font-weight-bold">count_income</label>
                            <input type="text" class="form-control @error('count_income') is-invalid @enderror" name="count_income" value="{{ old('count_income', $about->count_income) }}" placeholder="Masukkan count_income">
                        
                            <!-- error message untuk count_income -->
                            @error('count_income')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-md btn-primary">UPDATE</button>
                        <button type="reset" class="btn btn-md btn-warning">RESET</button>

                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
    
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'position' );
</script>
@push('scripts-plugins')

@endpush

@push('scripts')
<script>
    
 
</script>
@endpush
@endsection

